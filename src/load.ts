import { ComputationError } from './interfaces/types/FieldComputation';
import { FieldSet, getFieldSet } from './interfaces/types/FieldSet';
import { Rule } from './interfaces/types/Rule';
import { memoize } from './util';
import { prop } from 'fp-ts-ramda';
import * as Array from 'fp-ts/lib/Array';
import * as Either from 'fp-ts/lib/Either';
import * as f from 'fp-ts/lib/function';
import * as Option from 'fp-ts/lib/Option';
import { pipe } from 'fp-ts/lib/pipeable';
import * as TaskEither from 'fp-ts/lib/TaskEither';
import * as t from 'io-ts';
import { PathReporter } from 'io-ts/lib/PathReporter';
import YAML from 'yaml';
import path from 'path';
import * as fs from 'fs';

const lstat = TaskEither.taskify(
  (x: string, cb: (e: NodeJS.ErrnoException | null, files: fs.Stats) => void) =>
    fs.lstat(x, cb)
);
const readDir = TaskEither.taskify(
  (x: string, cb: (e: NodeJS.ErrnoException | null, files: string[]) => void) =>
    fs.readdir(x, cb)
);
const taskTraverse = Array.array.traverse(TaskEither.taskEither);

export const getBooks = (
  dir: string
): TaskEither.TaskEither<ComputationError, string[]> => {
  return pipe(
    dir,
    readDir,
    TaskEither.mapLeft(x => [x.message]),
    TaskEither.chain(d =>
      taskTraverse(d, file => {
        const filename = path.join(dir, file);
        return pipe(
          filename,
          lstat,
          TaskEither.mapLeft(x => [x.message]),
          TaskEither.chain(s => {
            if (s.isDirectory()) {
              return getBooks(filename);
            } else {
              return TaskEither.right([filename]);
            }
          })
        );
      })
    ),
    TaskEither.map(Array.flatten)
  );
};

const getRules = memoize(
  f.flow(
    TaskEither.taskify(fs.readFile),
    TaskEither.map(file => YAML.parse(file.toString('utf-8'))),
    TaskEither.map(
      f.flow(
        Option.fromNullable,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        Option.getOrElse((): any[] => [])
      )
    ),
    TaskEither.mapLeft(f.flow(prop('message'), x => [x])),
    TaskEither.map(t.array(Rule).decode),
    TaskEither.chain(
      f.flow(
        TaskEither.fromEither,
        TaskEither.mapLeft(f.flow(Either.left, PathReporter.report))
      )
    )
  )
);

export const load: (
  x: string[]
) => TaskEither.TaskEither<ComputationError, FieldSet> = f.flow(
  xs => taskTraverse(xs, getRules),
  TaskEither.map(f.flow(Array.flatten, getFieldSet))
);

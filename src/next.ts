import * as UnorderedSet from './interfaces/types/UnorderedSet';
import {
  Ref,
  Value,
  eqRef,
  getValueResolver,
  showRef,
} from './interfaces/types/Value';
import { Choice, combineChoices, eqChoice } from './interfaces/types/Choice';
import {
  ComputationError,
  calculate,
  combineComputations,
  eqFieldComputation,
} from './interfaces/types/FieldComputation';
import { FieldSet } from './interfaces/types/FieldSet';
import { Rule, toFieldComputation } from './interfaces/types/Rule';
import {
  UpdateResult,
  updateResultSemigroup,
} from './interfaces/types/UpdateResult';
import { pipe } from 'fp-ts/lib/pipeable';
import * as f from 'fp-ts/lib/function';
import * as Option from 'fp-ts/lib/Option';
import * as Map from 'fp-ts/lib/Map';
import * as Either from 'fp-ts/lib/Either';

const deriveValue = (
  root: Value,
  ref: Ref
): ((
  rules: Set<Rule>
) => Either.Either<
  ComputationError,
  Option.Option<Either.Either<Choice, Value>>
>) => {
  const resolve = getValueResolver(root);
  return f.flow(
    UnorderedSet.filter((r: Rule) => r.prerequisite.call(resolve)),
    UnorderedSet.partitionMap(
      eqChoice,
      eqFieldComputation
    )(toFieldComputation(resolve)),
    ({
      left,
      right,
    }): Either.Either<
      ComputationError,
      Option.Option<Either.Either<Choice, Value>>
    > => {
      const [choice, choices] = UnorderedSet.pop(eqChoice, left);
      const [computation, computations] = UnorderedSet.pop(
        eqFieldComputation,
        right
      );
      if (Option.isSome(choice)) {
        if (Option.isSome(computation)) {
          return Either.left(['Cannot combine choices and computations']);
        }
        return pipe(
          choices,
          UnorderedSet.reduce(Either.right(choice.value), combineChoices),
          Either.map(f.flow(Either.left, Option.some))
        );
      }
      if (Option.isSome(computation)) {
        return pipe(
          computations,
          UnorderedSet.reduce(
            Either.right(computation.value),
            combineComputations
          ),
          Either.chain(calculate),
          Either.chain(x =>
            Option.option.traverse(Either.either)(x, ref.set(root))
          ),
          Either.map(Option.map(Either.right))
        );
      }

      return Either.right(Option.none);
    }
  );
};

const update: (
  rules: FieldSet,
  root: Value
) => (ref: Ref) => Either.Either<ComputationError, UpdateResult> = (
  rules,
  root
) => ref => {
  return pipe(
    Map.lookup(eqRef)(ref, rules),
    Either.fromOption(() => [
      `Could not read rules for field ${showRef.show(ref)}`,
    ]),
    Either.chain(ruleset => {
      return pipe(
        ruleset.rules,
        deriveValue(root, ref),
        Either.mapLeft(es => [
          `Error deriving value for field ${showRef.show(ref)}`,
          ...es,
        ]),
        Either.map(
          Option.fold(
            // no rules were active for this field, so don't update anything and don't descend
            () => ({
              state: root,
              affectedFields: UnorderedSet.empty,
              questions: Map.empty,
            }),
            f.flow(
              Either.fold(
                choice => ({
                  state: root,
                  // TODO: We need to set this here because otherwise we will never go into a question field.
                  // However I don't know if it makes sense to say that the field is "affected" if we have not yet received an answer
                  affectedFields: ruleset.dependants,
                  questions: Map.singleton(ref, choice),
                }),
                state => ({
                  state,
                  affectedFields: ruleset.dependants,
                  questions: Map.empty,
                })
              )
            )
          )
        )
      );
    })
  );
};

export const updateAll: (
  rules: FieldSet,
  root: Value,
  refs: Set<Ref>
) => Either.Either<ComputationError, UpdateResult> = (rules, root, refs) => {
  const [maybeRef, rest] = UnorderedSet.pop(eqRef, refs);

  // TODO: there's probably a better way of doing all this:
  return pipe(
    maybeRef,
    Option.fold(
      () =>
        Either.right<ComputationError, UpdateResult>({
          state: root,
          affectedFields: UnorderedSet.empty,
          questions: Map.empty,
        }),
      f.flow(
        update(rules, root),
        Either.chain(result => {
          const joinSet = UnorderedSet.union(eqRef);
          const joinResults = Either.map((y: UpdateResult) =>
            updateResultSemigroup.concat(result, y)
          );
          return joinResults(
            updateAll(rules, result.state, joinSet(rest, result.affectedFields))
          );
        })
      )
    )
  );
};

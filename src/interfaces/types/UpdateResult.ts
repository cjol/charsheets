import { Choice, showChoice } from './Choice';
import { Ref, Value, eqRef, showRef, showValue } from './Value';
import * as Map from 'fp-ts/lib/Map';
import {
  Semigroup,
  getLastSemigroup,
  getStructSemigroup,
} from 'fp-ts/lib/Semigroup';
import { getUnionMonoid } from 'fp-ts/lib/Set';
import { Show } from 'fp-ts/lib/Show';

export interface UpdateResult {
  state: Value;
  affectedFields: Set<Ref>;
  questions: Map<Ref, Choice>;
}

export const updateResultSemigroup: Semigroup<UpdateResult> = getStructSemigroup(
  {
    state: getLastSemigroup<Value>(),
    affectedFields: getUnionMonoid(eqRef),
    questions: Map.getMonoid(eqRef, getLastSemigroup<Choice>()),
  }
);
export const indent = (depth: number): string => '  '.repeat(depth);

export const getShow = (depth = 0): Show<UpdateResult> => ({
  show: u =>
    `{
${indent(depth + 1)}questions: ${getPrettyShow(
      showRef,
      showChoice,
      depth + 1
    ).show(u.questions)}
${indent(depth + 1)}character: ${showValue.show(u.state)}
${indent(depth)}}`,
});

export function getPrettyShow<S, V>(
  s: Show<S>,
  v: Show<V>,
  depth = 0
): Show<Map<S, V>> {
  return {
    show: m => {
      let str = `Map{\n`;
      for (const [key, value] of m) {
        str += `${indent(depth + 1)}${s.show(key)} => ${v.show(value)}\n`;
      }
      str += `${indent(depth)}}`;
      return str;
    },
  };
}

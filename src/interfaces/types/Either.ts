import { Either, isLeft } from 'fp-ts/lib/Either';
import { Semigroup } from 'fp-ts/lib/Semigroup';

// TODO: ask a question about this pattern on SO
export function getValidatedSemigroup<A, B>(
  concatRights: (x: B, y: B) => Either<A, B>
): Semigroup<Either<A, B>> {
  return {
    concat: (x, y) => {
      if (isLeft(x)) { return x; }
      if (isLeft(y)) { return y; }
      return concatRights(x.right, y.right);
    },
  };
}

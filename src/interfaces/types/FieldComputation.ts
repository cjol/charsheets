import { ListValue, RecordValue, Value } from './Value';
import * as Array from 'fp-ts/lib/Array';
import * as Either from 'fp-ts/lib/Either';
import * as Eq from 'fp-ts/lib/Eq';
import * as f from 'fp-ts/lib/function';
import * as Monoid from 'fp-ts/lib/Monoid';
import * as Option from 'fp-ts/lib/Option';
import { pipe } from 'fp-ts/lib/pipeable';

/**
 * FieldComputation is:
 *   - struct of
 *     - base :: Option<Value>
 *     - modifier :: Option<Value> => Option<Value>
 *  - Part of Semigroup + Magma:
 *    - Base Value can be folded under `last` (or some other semantics TBC)
 *    - Modifier can be folded under `compose`/`flow`
 *    - Part of monoid with `empty` = { base: none, modifier: identity }
 *  - has `calculate`: () => Option<Value>
 */
export type ComputationError = string[];
export const computationErrorMonoid: Monoid.Monoid<ComputationError> = Array.getMonoid<
  string
>();

export const eqFieldComputation = pipe(
  Eq.eqString,
  Eq.contramap<string, FieldComputation>(q => q.id)
);

export interface FieldComputation {
  id: string;
  base: Option.Option<Value>;
  modifier: (f: Value) => Either.Either<ComputationError, Value>;
}

export const fieldComputationMonoid = Monoid.getStructMonoid<FieldComputation>({
  id: Monoid.monoidString,
  base: Option.getLastMonoid<Value>(),
  modifier: {
    empty: Either.right,
    // TODO: there's probably a semigroup for this already... (or indeed a whole Eithermonoid)
    concat: (f1, f2) => f.flow(f1, Either.chain(f2)),
  },
});

export const combineComputations = (
  curr: Either.Either<ComputationError, FieldComputation>,
  x: FieldComputation
): Either.Either<ComputationError, FieldComputation> => {
  return pipe(
    curr,
    Either.chain((y: FieldComputation) => {
      // ensure we have at most one base for the rule
      if (Option.isSome(x.base) && Option.isSome(y.base)) {
        let mergedBase: Value | undefined;
        if (ListValue.is(x.base.value) && ListValue.is(y.base.value)) {
          // actually we can combine these bases
          mergedBase = { list: x.base.value.list.concat(y.base.value.list) };
        }
        if (RecordValue.is(x.base.value) && RecordValue.is(y.base.value)) {
          // actually we can combine these bases
          mergedBase = {
            record: { ...x.base.value.record, ...y.base.value.record },
          };
        }
        if (mergedBase) {
          return Either.right(
            fieldComputationMonoid.concat(
              { ...x, base: Option.none },
              { ...y, base: Option.some(mergedBase) }
            )
          );
        }

        return Either.left([
          'Cannot combine bases from ' + x.id + ' and ' + y.id,
        ]);
      }

      return Either.right(fieldComputationMonoid.concat(x, y));
    })
  );
};

// Returns right(none) when no base has been set - so the calculation is not runnable
// TODO: it's probably possible and would be slightly more accurate to have Option<Either> but this caused problems I couldn't be bothered to fix
export function calculate(
  field: FieldComputation
): Either.Either<ComputationError, Option.Option<Value>> {
  return pipe(field.base, x =>
    Option.option.traverse(Either.either)(x, field.modifier)
  );
}

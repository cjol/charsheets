import { Rule, eqRule } from './Rule';
import { Ref, eqRef } from './Value';
import { getStructMonoid } from 'fp-ts/lib/Monoid';
import { getUnionMonoid } from 'fp-ts/lib/Set';

/**
 * Ruleset is:
 *  - collecting all information for a given field
 *  - struct of
 *    - rules :: Set<string>
 *    - dependants :: Set<Rule>
 *  - Part of Magma/Semigroup + Monoid as struct:
 *    - dependants folded under union Monoid
 *    - rules folded under union Monoid
 */

export interface Ruleset {
  dependants: Set<Ref>;
  rules: Set<Rule>;
}

export const rulesetMonoid = getStructMonoid<Ruleset>({
  dependants: getUnionMonoid(eqRef),
  rules: getUnionMonoid(eqRule),
});

import { Ref, ValueResolver, eqRef } from './Value';
import { eqOptValue } from './Value/OptValue';
import { SerialisedPrerequisite } from '../serialised/prerequisite';
import { map } from 'fp-ts/lib/Array';
import { either } from 'fp-ts/lib/Either';
import { Predicate, flow } from 'fp-ts/lib/function';
import {
  Monoid,
  fold,
  getFunctionMonoid,
  getStructMonoid,
  monoidAll,
  monoidAny,
} from 'fp-ts/lib/Monoid';
import { isSome } from 'fp-ts/lib/Option';
import * as Set from 'fp-ts/lib/Set';
import * as t from 'io-ts';

/**
 * Prerequisite is:
 *  - Predicate on Character
 *  - not part of Eq / Setoid (can't be checked for equality)
 *  - not part of Ord (can't be ordered)
 *  - part of Semigroup + Magma under conjunction (can be concatted associatively with compose)
 *    - part of Monoid (has empty with constTrue)
 *  - part of Semigroup + Magma under disjunction (can be concatted associatively with compose)
 *    - part of Monoid (has empty with constFalse)
 *   - could be functor - can map over inner value
 *
 * When we load individual prerequisites, they will be loaded in disjunctive normal form Predicate[][].
 * This can be folded with flow(map(foldAll), foldAny)
 */
export interface Prerequisite {
  call: Predicate<ValueResolver>;
  dependencies: Set<Ref>;
}

export const Prerequisite = new t.Type<Prerequisite, null, unknown>(
  'Prerequisite',
  (u): u is Prerequisite => typeof u === 'function', // TODO: this is a bit jammy (but not planning to use)
  (x, ctx) =>
    either.chain(SerialisedPrerequisite.validate(x, ctx), serialised => {
      if ('equals' in serialised) {
        return t.success<Prerequisite>({
          call: v =>
            eqOptValue.equals(v(serialised.value), v(serialised.equals)),
          dependencies: Set.fromArray(eqRef)(
            [serialised.value, serialised.equals].filter(Ref.is)
          ),
        });
      }

      // must be isDefinedPrerequisite at this stage
      return t.success<Prerequisite>({
        call: v => isSome(v(serialised.isDefined)),
        dependencies: Set.fromArray(eqRef)(
          [serialised.isDefined].filter(Ref.is)
        ),
      });
    }),
  () => null // TODO: again, a bit jammy but not planning to use
);

export const prerequisiteDNF = t.array(t.array(Prerequisite));

function getMonoidPrerequisite(
  boolMonoid: Monoid<boolean>
): Monoid<Prerequisite> {
  return getStructMonoid<Prerequisite>({
    call: getFunctionMonoid(boolMonoid)(),
    dependencies: Set.getUnionMonoid(eqRef),
  });
}

const foldAll = fold(getMonoidPrerequisite(monoidAll));
const foldAny = fold(getMonoidPrerequisite(monoidAny));

export const loadPrerequisitesDNF = flow(map(foldAll), foldAny);

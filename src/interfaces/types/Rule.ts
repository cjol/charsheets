import { Choice } from './Choice';
import { ComputationError, FieldComputation } from './FieldComputation';
import { Prerequisite, loadPrerequisitesDNF } from './Prerequisite';
import * as UnorderedSet from './UnorderedSet';
import { Ref, Value, ValueResolver, eqRef , ValueRef } from './Value';
import { PrimValue } from './Value/PrimValue';
import {
  CalculateRule,
  CalculateStep,
  SerialisedRule,
} from '../serialised/rule';
import * as Array from 'fp-ts/lib/Array';
import * as Either from 'fp-ts/lib/Either';
import { Eq, contramap as eqContramap, eqString } from 'fp-ts/lib/Eq';
import { flow } from 'fp-ts/lib/function';
import * as Option from 'fp-ts/lib/Option';
import { Ord, contramap as ordContramap, ordString } from 'fp-ts/lib/Ord';
import { pipe } from 'fp-ts/lib/pipeable';
import * as t from 'io-ts';


/**
 * Rule is:
 *  - struct of
 *    - id: string
 *    - field: string
 *    - Prerequisite
 *    | BaseRule(Value)
 *    | ModifierRule(Value => Value)
 *  - part of Eq / Setoid (can be checked for equality using rule ID)
 *  - Not meaningfully part of Ord (could arguably be ordered by string ID)
 *  - Not meaningfully concattable
 *  - transformable to Field
 */

interface RuleCommon {
  id: string;
  field: Ref;
  prerequisite: Prerequisite;
  dependencies: Set<Ref>;
}

type BaseRule = RuleCommon & {
  type: 'base';
  value: (v: ValueResolver) => Option.Option<Value>;
};

interface ModifierRule extends RuleCommon {
  type: 'modifier';
  base: Option.Option<(v: ValueResolver) => Option.Option<Value>>;
  modifier: (
    v: ValueResolver
  ) => (v: Value) => Either.Either<ComputationError, Value>;
}

interface QuestionRule extends RuleCommon {
  type: 'choice';
  choice: Choice;
}

export type Rule = BaseRule | ModifierRule | QuestionRule;
export const Rule = new t.Type<Rule, null, unknown>(
  'Rule',
  (u): u is Rule => !!u && false, // TODO: this is a bit jammy (but not planning to use)
  (x, ctx) => {
    return Either.either.chain(
      SerialisedRule.validate(x, ctx),
      (serialised): t.Validation<Rule> => {
        const common = {
          id: serialised.id,
          field: serialised.field,
          prerequisite: loadPrerequisitesDNF(serialised.prerequisites || [[]]),
        };
        switch (serialised.type) {
          case 'calculate':
            return t.success({
              ...common,
              type: 'modifier',
              base: pipe(
                Option.fromNullable(serialised.base),
                Option.map(val => res => res(val))
              ),
              modifier: v => getCalculationModifier(serialised, v),
              dependencies: getCalculationDependencies(serialised),
            });

          case 'set':
            return t.success({
              ...common,
              type: 'base',
              value: v => v(serialised.value),
              dependencies: getValueDependencies(serialised.value),
            });

          case 'group':
            return t.success({
              ...common,
              type: 'base',
              dependencies: UnorderedSet.empty,
              value: () => Option.some({ prim: true }),
            });

          case 'choice':
            return t.success({
              ...common,
              type: 'choice',
              dependencies: getChoiceDependencies(serialised.choice),
              choice: {
                ...serialised.choice,
                id: serialised.id,
              },
            });
        }
      }
    );
  },
  () => null // TODO: again, a bit jammy but not planning to use
);

export const eqRule: Eq<Rule> = pipe(
  eqString,
  eqContramap(r => r.id)
);
export const ordRule: Ord<Rule> = pipe(
  ordString,
  ordContramap(r => r.id)
);

export const toFieldComputation = (v: ValueResolver) => (
  r: Rule
): Either.Either<Choice, FieldComputation> => {
  switch (r.type) {
    case 'base':
      return Either.right({ base: r.value(v), modifier: Either.right, id: r.id });
    case 'modifier':
      return Either.right({
        base: pipe(
          r.base,
          Option.chain(b => b(v))
        ),
        modifier: r.modifier(v),
        id: r.id,
      });
    case 'choice':
      return Either.left({ ...r.choice, id: r.id });
  }
};

export function getValueDependencies(v: ValueRef): Set<Ref> {
  if (Ref.is(v)) {
    const result = UnorderedSet.singleton(v);
    return result;
  }
  return UnorderedSet.empty;
}

function getChoiceDependencies(c: Choice): Set<Ref> {
  switch (c.type) {
    case 'freetext':
    case 'number':
      return UnorderedSet.empty;
    case 'select':
      if (Ref.is(c.targets)) {
        return UnorderedSet.singleton(c.targets);
      }
      return UnorderedSet.empty;
  }
}

function getCalculationDependencies(rule: CalculateRule): Set<Ref> {
  return pipe(
    rule,
    r => r.calculation,
    Array.foldMap(UnorderedSet.getUnionMonoid(eqRef))(c => {
      switch (c[0]) {
        case 'add':
        case 'subtract':
        case 'divide':
        case 'multiply':
          return getValueDependencies(c[1]);
        default:
          return UnorderedSet.empty;
      }
    }),
    x =>
      rule.base
        ? UnorderedSet.union(eqRef)(x, getValueDependencies(rule.base))
        : x
  );
}

type ValueFunction = (
  i: Either.Either<ComputationError, Value>
) => Either.Either<ComputationError, Value>;

// TODO: replace calculations with proper expression parsing and evaluation
function getCalculationModifier(
  rule: CalculateRule,
  resolve: ValueResolver
): (y: Value) => Either.Either<ComputationError, Value> {
  const wrapEither = Either.chain(
    (x: Value): Either.Either<ComputationError, number> => {
      if (PrimValue.is(x) && typeof x.prim === 'number') {
        return Either.right(x.prim);
      }
      return Either.left(['Cannot calculate from non-number ' + x]);
    }
  );

  const wrapRef: (
    ref: ValueRef
  ) => Either.Either<ComputationError, number> = flow(
    resolve,
    Either.fromOption(() => ['Cannot calculate from null reference']),
    wrapEither
  );

  const lift = (param: ValueRef, f: (x: number) => (y: number) => Value) => (
    prev: Either.Either<ComputationError, Value>
  ): Either.Either<ComputationError, Value> =>
    pipe(wrapEither(prev), Either.map(f), Either.ap(wrapRef(param)));

  // TODO: this could presumably be simplified with `compose` and `identity`
  const foldCompose = Array.foldMap<
    (
      x: Either.Either<ComputationError, Value>
    ) => Either.Either<ComputationError, Value>
  >({
    empty: x => x,
    concat: (f1, f2) => x => f2(f1(x)),
  });

  return pipe(
    rule.calculation,
    foldCompose(
      (next: CalculateStep): ValueFunction => {
        switch (next[0]) {
          case 'add':
            return lift(next[1], prev => param => ({
              prim: prev + param,
            }));
          case 'subtract':
            return lift(next[1], prev => param => ({
              prim: prev - param,
            }));
          case 'divide':
            return lift(next[1], prev => param => ({
              prim: prev / param,
            }));
          case 'multiply':
            return lift(next[1], prev => param => ({
              prim: prev * param,
            }));
          case 'round-down':
            return flow(
              wrapEither,
              Either.map(prev => ({ prim: Math.floor(prev) }))
            );
          case 'round-up':
            return flow(
              wrapEither,
              Either.map(prev => ({ prim: Math.ceil(prev) }))
            );
        }
      }
    ),
    mod => flow(Either.right, mod)
  );
}

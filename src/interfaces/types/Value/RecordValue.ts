import { Value, eqValue, showValue } from '.';
import { SerializedRecordValue } from './Serialized';
import * as Either from 'fp-ts/lib/Either';
import { contramap as eqContramap } from 'fp-ts/lib/Eq';
import { pipe } from 'fp-ts/lib/pipeable';
import * as Record from 'fp-ts/lib/Record';
import { Show } from 'fp-ts/lib/Show';
import * as t from 'io-ts';

export interface RecordValue {
  record: { [k: string]: Value };
}

export const RecordValue = new t.Type<RecordValue, SerializedRecordValue>(
  'RecordValue',
  (x): x is RecordValue =>
    typeof x === 'object' &&
    x !== null &&
    'record' in x &&
    !('$ref' in (x as RecordValue).record) &&
    Object.values((x as RecordValue).record).every(Value.is),
  (x, ctx) => {
    if (typeof x !== 'object' || x === null || '$ref' in x) {
      return t.failure(x, ctx);
    }

    return pipe(
      x as SerializedRecordValue,
      Record.traverse(Either.either)(Value.decode),
      Either.map(record => ({ record }))
    );
  },
  a => Record.record.map(a.record, Value.encode)
);

export const eqRecordValue = eqContramap((v: RecordValue) => v.record)(
  Record.getEq(eqValue)
);

export const showRecordValue: Show<RecordValue> = {
  show: v => Record.getShow(showValue).show(v.record),
};

export const insertAt = (k: string, r: RecordValue) => (
  a: Value
): RecordValue => ({
  record: Record.insertAt(k, a)(r.record),
});

export const empty: RecordValue = { record: {} };

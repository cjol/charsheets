import { Value } from '.';
import { RecordValue, insertAt } from './RecordValue';
import { SerializedRef } from './Serialized';
import { ComputationError } from '../FieldComputation';
import { array } from 'fp-ts/lib/Array';
import { Either, chain, left, right } from 'fp-ts/lib/Either';
import { Eq, contramap, eqString } from 'fp-ts/lib/Eq';
import { flow } from 'fp-ts/lib/function';
import { Monoid } from 'fp-ts/lib/Monoid';
import * as Option from 'fp-ts/lib/Option';
import { Ord, contramap as ordContramap, ordString } from 'fp-ts/lib/Ord';
import { pipe } from 'fp-ts/lib/pipeable';
import { Show } from 'fp-ts/lib/Show';
import * as t from 'io-ts';

export interface Ref {
  name: string;
  get: (root: Value) => Option.Option<Value>;
  set: (root: Value) => (val: Value) => Either<ComputationError, Value>;
}

export const fromStringKey = (s: string): Ref => ({
  name: s,
  get: root => {
    if (RecordValue.is(root)) {
      return Option.fromNullable(root.record[s]);
    }
    // TODO: maybe return an error? We just tried to descend into a non-record type
    return Option.none;
  },
  set: root => {
    if (RecordValue.is(root)) {
      return flow(insertAt(s, root), right);
    }
    // TODO: maybe return an error? We just tried to descend into a non-record type
    return () => left(['Tried to insert inside non-record field at ' + s]);
  },
});

export const monoidRef: Monoid<Ref> = {
  empty: {
    name: '',
    get: Option.some,
    set: () => right,
  },
  concat: (r1, r2) => {
    if (!r1.name) {
      return r2;
    }
    if (!r2.name) {
      return r1;
    }
    return {
      name: r1.name + '.' + r2.name,
      get: flow(r1.get, Option.chain(r2.get)),
      set: root => v =>
        pipe(
          r1.get(root),
          Option.getOrElse((): Value => ({ record: {} })),
          subroot => r2.set(subroot)(v),
          chain(r1.set(root))
        ),
    };
  },
};

export const Ref: t.Type<Ref, SerializedRef> = new t.Type<Ref, SerializedRef>(
  'ValueRef',

  (x): x is Ref =>
    typeof x === 'object' && !!x && 'get' in x && 'set' in x && 'name' in x,

  (x, ctx) => {
    if (typeof x !== 'object' || x === null) {
      return t.failure(x, ctx);
    }

    if ('$ref' in x && typeof (x as SerializedRef).$ref === 'string') {
      return t.success(
        array.foldMap(monoidRef)(
          (x as SerializedRef).$ref.split('.'),
          fromStringKey
        )
      );
    }
    return t.failure(x, ctx);
  },

  a => ({ $ref: a.name })
);

export const eqRef: Eq<Ref> = contramap((r: Ref) => r.name)(eqString);

export const showRef: Show<Ref> = {
  show: r => r.name,
};

export const ordRef: Ord<Ref> = pipe(
  ordString,
  ordContramap(r => r.name)
);

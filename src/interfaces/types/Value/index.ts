import { ListValue, eqListValue, showListValue } from './ListValue';
import { PrimValue, eqPrimValue, showPrimValue } from './PrimValue';
import { RecordValue, eqRecordValue, showRecordValue } from './RecordValue';
import { SerializedValue } from './Serialized';
import { Ref } from './Ref';
import * as Either from 'fp-ts/lib/Either';
import { Eq } from 'fp-ts/lib/Eq';
import { pipe } from 'fp-ts/lib/pipeable';
import { Show } from 'fp-ts/lib/Show';
import * as t from 'io-ts';

export type Value = PrimValue | ListValue | RecordValue;

export const Value: t.Type<Value, SerializedValue> = new t.Type<
  Value,
  SerializedValue
>(
  'Value',
  (x): x is Value => {
    if (PrimValue.is(x)) {
      return true;
    }
    if (ListValue.is(x)) {
      return true;
    }
    if (RecordValue.is(x)) {
      return true;
    }

    return false;
  },
  (x, ctx) => {
    // fail fast on refs
    if (Ref.is(x)) {
      return t.failure(x, ctx);
    }

    return pipe(
      PrimValue.validate(x, ctx) as Either.Either<t.Errors, Value>,
      Either.fold(
        () => ListValue.validate(x, ctx) as Either.Either<t.Errors, Value>,
        Either.right
      ),
      Either.fold(
        () => RecordValue.validate(x, ctx) as Either.Either<t.Errors, Value>,
        Either.right
      )
    );
  },
  a => {
    if (PrimValue.is(a)) {
      return PrimValue.encode(a);
    }
    if (ListValue.is(a)) {
      return ListValue.encode(a);
    }
    // must be record
    return RecordValue.encode(a);
  }
);

export const eqValue: Eq<Value> = {
  equals: (a, b) => {
    if (PrimValue.is(a) && PrimValue.is(b)) {
      return eqPrimValue.equals(a, b);
    }
    if (ListValue.is(a) && ListValue.is(b)) {
      return eqListValue.equals(a, b);
    }
    if (RecordValue.is(a) && RecordValue.is(b)) {
      return eqRecordValue.equals(a, b);
    }
    return false;
  },
};

export const showValue: Show<Value> = {
  show: v => {
    if (PrimValue.is(v)) {
      return showPrimValue.show(v);
    }
    if (ListValue.is(v)) {
      return showListValue.show(v);
    }
    // must be record
    return showRecordValue.show(v);
  },
};

export * from './ListValue';
export * from './OptValue';
export * from './PrimValue';
export * from './RecordValue';
export * from './Ref';
export * from './ValueRef';

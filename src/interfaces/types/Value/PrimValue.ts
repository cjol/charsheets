import { SerializedPrimValue } from './Serialized';
import { contramap, strictEqual } from 'fp-ts/lib/Eq';
import { Show } from 'fp-ts/lib/Show';
import * as t from 'io-ts';

export interface PrimValue {
  prim: string | number | boolean;
}

export const PrimValue = new t.Type<PrimValue, SerializedPrimValue>(
  'PrimValue',
  (x): x is PrimValue => {
    if (typeof x !== 'object' || x === null || !('prim' in x)) { return false; }
    switch (typeof (x as PrimValue).prim) {
      case 'string':
      case 'number':
      case 'boolean':
        return true;

      default:
        return false;
    }
  },
  (x, ctx) => {
    switch (typeof x) {
      case 'string':
      case 'number':
      case 'boolean':
        return t.success({ prim: x });
    }
    return t.failure(x, ctx);
  },
  a => a.prim
);

export const eqPrimValue = contramap((x: PrimValue) => x.prim)({
  equals: strictEqual,
});

export const showPrimValue: Show<PrimValue> = { show: x => '' + x.prim };

import { Value, eqValue, showValue } from '.';
import { Ref, eqRef, showRef } from './Ref';
import { Eq } from 'fp-ts/lib/Eq';
import { Option, some } from 'fp-ts/lib/Option';
import { Show } from 'fp-ts/lib/Show';
import * as t from 'io-ts';
export type ValueRef = Value | Ref;

export const ValueRef = t.union([Value, Ref]);

export const showValueOrRef: Show<ValueRef> = {
  show: a => {
    if (Ref.is(a)) {
      return showRef.show(a);
    }
    return showValue.show(a);
  },
};

export const eqValueOrRef: Eq<ValueRef> = {
  equals: (a, b) => {
    if (Ref.is(a) && Ref.is(b)) { return eqRef.equals(a, b); }
    if (!Ref.is(a) && !Ref.is(b)) { return eqValue.equals(a, b); }
    return false;
  },
};

export type ValueResolver = (v: ValueRef) => Option<Value>;

export function getValueResolver(root: Value): ValueResolver {
  return (ref: ValueRef) => {
    if (Ref.is(ref)) {
      return ref.get(root);
    }
    return some(ref);
  };
}

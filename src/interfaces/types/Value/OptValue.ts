import { Value, eqValue } from '.';
import { getEq , Option } from 'fp-ts/lib/Option';


export type OptValue = Option<Value>;

export const eqOptValue = getEq(eqValue);

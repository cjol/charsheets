export type SerializedValue =
  | SerializedPrimValue
  | SerializedRecordValue
  | SerializedListValue;

export type SerializedPrimValue = string | number | boolean;

export interface SerializedRecordValue {
  [k: string]: SerializedValue;
}

export type SerializedListValue = Array<SerializedValue>

export interface SerializedRef {
  $ref: string;
}

export type SerializedValueRef = SerializedRef | SerializedValue;

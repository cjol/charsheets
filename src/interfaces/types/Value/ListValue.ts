import { Value, eqValue, showValue } from '.';
import { SerializedListValue } from './Serialized';
import {
  getEq as ArrayGetEq,
  getShow as ArrayGetShow,
  array,
} from 'fp-ts/lib/Array';
import * as Either from 'fp-ts/lib/Either';
import { contramap as eqContramap } from 'fp-ts/lib/Eq';
import { pipe } from 'fp-ts/lib/pipeable';
import { Show } from 'fp-ts/lib/Show';
import * as t from 'io-ts';

export interface ListValue {
  list: Value[];
}

export const ListValue = new t.Type<ListValue, SerializedListValue>(
  'ListValue',
  (x): x is ListValue =>
    typeof x === 'object' &&
    x !== null &&
    'list' in x &&
    (x as ListValue).list.every(Value.is),
  (x, ctx) => {
    if (!Array.isArray(x)) { return t.failure(x, ctx); }
    const xs = x as SerializedListValue;
    return pipe(
      array.traverse(Either.either)(xs, v => Value.validate(v, ctx)),
      Either.map(list => ({ list }))
    );
  },
  a => a.list.map(Value.encode)
);

export const eqListValue = eqContramap((v: ListValue) => v.list)(
  ArrayGetEq(eqValue)
);

export const showListValue: Show<ListValue> = {
  show: l => ArrayGetShow(showValue).show(l.list),
};

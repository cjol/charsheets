import { Rule } from './Rule';
import { Ruleset, rulesetMonoid } from './Ruleset';
import { Ref, eqRef, ordRef } from './Value';
import * as Array from 'fp-ts/lib/Array';
import * as Map from 'fp-ts/lib/Map';
import { pipe } from 'fp-ts/lib/pipeable';
import * as Set from 'fp-ts/lib/Set';

// TODDO: FieldSet should become tre
/**
 * FieldSet is:
 *  - map of (field :: string) => Ruleset
 *  - not part of Eq / Setoid (can't be checked for equality)
 *  - Not meaningfully part of ord
 *  - Part of Magma/Semigroup + Monoid with RulesetMonoid
 */
export type FieldSet = Map<Ref, Ruleset>
const fieldsetMonoid = Map.getMonoid(eqRef, rulesetMonoid);

const getDeps = (r: Rule): Set<Ref> => {
  // prerequisite dependencies:
  return Array.reduce(
    // explicit dependencies (optional - probably not needed if all others are better):
    r.dependencies,
    Set.union(eqRef)
  )([
    // implicit prerequisite dependencies
    r.prerequisite.dependencies,
  ]);
};

const fromRule = (r: Rule): FieldSet =>
  fieldsetMonoid.concat(
    // add rule itself
    Map.singleton(r.field, {
      dependants: Set.empty,
      rules: Set.singleton(r),
    }),
    // also add stubs marking dependencies
    pipe(
      getDeps(r),
      Set.foldMap(
        ordRef,
        fieldsetMonoid
      )(d =>
        Map.singleton(d, {
          dependants: Set.singleton(r.field),
          rules: Set.empty,
        })
      )
    )
  );

export const getFieldSet = Array.foldMap(fieldsetMonoid)(fromRule);

import { ComputationError } from './FieldComputation';
import * as UnorderedSet from './UnorderedSet';
import { ListValue } from './Value';
import { ValueRef, showValueOrRef } from './Value/ValueRef';
import { SerialisedChoice } from '../serialised/choice';
import { Either, chain, either, left, right } from 'fp-ts/lib/Either';
import * as Eq from 'fp-ts/lib/Eq';
import { pipe } from 'fp-ts/lib/pipeable';
import { semigroupString } from 'fp-ts/lib/Semigroup';
import * as Set from 'fp-ts/lib/Set';
import { Show, showString } from 'fp-ts/lib/Show';
import * as t from 'io-ts';

interface ChoiceCommon {
  id: string;
}

interface NumberChoice extends ChoiceCommon {
  type: 'number';
}

interface FreeTextChoice extends ChoiceCommon {
  type: 'freetext';
}

interface SelectChoice extends ChoiceCommon {
  type: 'select';
  // TODO: Something about making 0 selections?
  mode:
    | 'one-to-one' // list, unique targets
    | 'one-to-many' // checkbox, unique targets
    | 'many-to-one' // list, all targets
    | 'many-to-many'; // checkbox, all targets
  targets: ValueRef;
  sources: Set<string>;
}

const stringSetUnion = Set.union(Eq.eqString);
const stringSetShow = UnorderedSet.getShow(showString);
const makeStringSet = Set.fromArray(Eq.eqString);

export type Choice = SelectChoice | FreeTextChoice | NumberChoice;
export const Choice = new t.Type<Choice, null, unknown>(
  'Choice',
  (u): u is Choice => !!u && false, // TODO: this is a bit jammy (but not planning to use)
  (x, ctx) =>
    either.chain(
      SerialisedChoice.validate(x, ctx),
      (serialised): t.Validation<Choice> => {
        const common = { id: '' };
        switch (serialised.type) {
          case 'freetext':
            return t.success<FreeTextChoice>({ ...common, type: 'freetext' });
          case 'number':
            return t.success<NumberChoice>({ ...common, type: 'number' });
          // TODO: sources and targets should be ValueRef so we can pick based on other values
          case 'select':
            return t.success<SelectChoice>({
              ...common,
              type: 'select',
              mode: serialised.mode || 'many-to-many',
              sources: makeStringSet(serialised.sources || []),
              targets: serialised.targets || { list: [] },
            });
        }
      }
    ),
  () => null // TODO: again, a bit jammy but not planning to use
);

export const eqChoice = pipe(
  Eq.eqString,
  Eq.contramap<string, Choice>(q => q.id)
);

export const showChoice: Show<Choice> = {
  show: c => {
    switch (c.type) {
      case 'number':
      case 'freetext':
        return c.type;
      case 'select':
        return `Select{ ${stringSetShow.show(
          c.sources
        )} => ${showValueOrRef.show(c.targets)} }`;
    }
  },
};

export const combineChoices = (
  curr: Either<ComputationError, Choice>,
  x: Choice
): Either<ComputationError, Choice> => {
  return chain(
    (y: Choice): Either<ComputationError, Choice> => {
      const id = semigroupString.concat(x.id, y.id);

      if (x.type === 'number' && y.type === 'number') {
        return right({ id, type: 'number' });
      }
      if (x.type === 'freetext' && y.type === 'freetext') {
        return right({ id, type: 'freetext' });
      }
      if (x.type === 'select' && y.type === 'select') {
        if (x.mode !== (y as SelectChoice).mode) {
          return left([
            `Cannot combine select modes ${x.mode} and ${
              (y as SelectChoice).mode
            }`,
          ]);
        }

        if (ListValue.is(x.targets) && ListValue.is(y.targets)) {
          return right({
            id,
            type: 'select',
            mode: x.mode,
            targets: { list: x.targets.list.concat(y.targets.list) },
            sources: stringSetUnion(x.sources, (y as SelectChoice).sources),
          });
        }
        return left([`Haven't handled this case yet sorry.`]);
      }

      return left([`Cannot combine choice types ${x.type} and ${y.type}`]);
    }
  )(curr);
};

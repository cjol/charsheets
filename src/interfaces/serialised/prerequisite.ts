import { ValueRef } from '../types/Value';
import * as t from 'io-ts';

const EqualsPrerequisite = t.interface(
  {
    value: ValueRef,
    equals: ValueRef,
  },
  'EqualsPrerequisite'
);

const IsDefinedPrerequisite = t.interface(
  {
    isDefined: ValueRef,
  },
  'IsDefinedPrerequisite'
);

export const SerialisedPrerequisite = t.union([
  IsDefinedPrerequisite,
  EqualsPrerequisite,
]);

export type SerialisedPrerequisite = t.TypeOf<typeof SerialisedPrerequisite>;

import { ValueRef } from '../types/Value/ValueRef';
import * as t from 'io-ts';

const SerialisedSelectChoice = t.intersection(
  [
    t.interface({
      type: t.keyof({ select: null }),
    }),
    t.partial({
      sources: t.array(t.string),
      targets: ValueRef,
      mode: t.keyof({
        'one-to-one': null,
        'one-to-many': null,
        'many-to-one': null,
        'many-to-many': null,
      }),
    }),
  ],
  'SerialisedSelectChoice'
);
export type SerialisedSelectChoice = t.TypeOf<typeof SerialisedSelectChoice>

const SerialisedNumberChoice = t.interface(
  {
    type: t.keyof({ number: null }),
  },
  'SerialisedNumberChoice'
);

const SerialisedFreeTextChoice = t.interface(
  {
    type: t.keyof({ freetext: null }),
  },
  'SerialisedFreeTextChoice'
);
export type SerialisedFreeTextChoice = t.TypeOf<typeof SerialisedFreeTextChoice>

export const SerialisedChoice = t.union(
  [SerialisedSelectChoice, SerialisedFreeTextChoice, SerialisedNumberChoice],
  'SerialisedChoice'
);
export type SerialisedChoice = t.TypeOf<typeof SerialisedChoice>;

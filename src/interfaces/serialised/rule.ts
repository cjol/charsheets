import { Choice } from '../types/Choice';
import { Prerequisite } from '../types/Prerequisite';
import { Ref } from '../types/Value';
import { ValueRef } from '../types/Value/ValueRef';
import * as t from 'io-ts';

const BaseRule = t.intersection(
  [
    t.interface({
      field: Ref,
      id: t.string,
    }),
    t.partial({
      prerequisites: t.array(t.array(Prerequisite, 'Array<Prerequisite>')),
    }),
  ],
  'BaseRule'
);
type BaseRule = t.TypeOf<typeof BaseRule>

const CalculateRule = t.intersection(
  [
    BaseRule,
    t.interface({
      type: t.keyof({ calculate: null }),
      calculation: t.array(
        t.union([
          t.tuple([t.keyof({ add: null }), ValueRef]),
          t.tuple([t.keyof({ subtract: null }), ValueRef]),
          t.tuple([t.keyof({ divide: null }), ValueRef]),
          t.tuple([t.keyof({ multiply: null }), ValueRef]),
          t.tuple([t.keyof({ 'round-up': null })]),
          t.tuple([t.keyof({ 'round-down': null })]),
        ])
      ),
    }),
    t.partial({
      base: ValueRef,
    }),
  ],
  'CalculateRule'
);
export type CalculateRule = t.TypeOf<typeof CalculateRule>
export type CalculateStep = CalculateRule['calculation'][number];

const GroupRule = t.intersection(
  [
    BaseRule,
    t.interface({
      type: t.keyof({ group: null }),
    }),
  ],
  'GroupRule'
);
type GroupRule = t.TypeOf<typeof GroupRule>

const SetRule = t.intersection(
  [
    BaseRule,
    t.interface({
      type: t.keyof({ set: null }),
      value: ValueRef,
    }),
  ],
  'SetRule'
);
type SetRule = t.TypeOf<typeof SetRule>

const ChoiceRule = t.intersection(
  [
    BaseRule,
    t.interface({
      type: t.keyof({ choice: null }),
      choice: Choice,
    }),
  ],
  'ChoiceRule'
);
type ChoiceRule = t.TypeOf<typeof ChoiceRule>

export const SerialisedRule = t.union(
  [CalculateRule, SetRule, GroupRule, ChoiceRule],
  'Rule'
);
export type SerialisedRule = t.TypeOf<typeof SerialisedRule>;

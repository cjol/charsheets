import { Choice } from './interfaces/types/Choice';
import { ComputationError } from './interfaces/types/FieldComputation';
import { FieldSet } from './interfaces/types/FieldSet';
import * as UnorderedSet from './interfaces/types/UnorderedSet';
import { UpdateResult } from './interfaces/types/UpdateResult';
import {
  ListValue,
  Ref,
  Value,
  ValueResolver,
  eqRef,
  fromStringKey,
  getValueResolver,
  monoidRef,
  ordRef,
  showRef,
  showValue,
} from './interfaces/types/Value';
import * as RecordValue from './interfaces/types/Value/RecordValue';
import { getBooks, load } from './load';
import { updateAll } from './next';
import * as Array from 'fp-ts/lib/Array';
import * as Console from 'fp-ts/lib/Console';
import * as Either from 'fp-ts/lib/Either';
import { Eq } from 'fp-ts/lib/Eq';
import { flow } from 'fp-ts/lib/function';
import * as Map from 'fp-ts/lib/Map';
import * as Option from 'fp-ts/lib/Option';
import { pipe } from 'fp-ts/lib/pipeable';
import * as Set from 'fp-ts/lib/Set';
import * as Task from 'fp-ts/lib/Task';
import * as TaskEither from 'fp-ts/lib/TaskEither';
import { Answers, DistinctQuestion, prompt } from 'inquirer';
import path from 'path';

const promptTask = (
  q: DistinctQuestion[]
): TaskEither.TaskEither<ComputationError, Answers> =>
  TaskEither.tryCatch(
    () => prompt(q),
    x => [String(x)]
  );

const setup: TaskEither.TaskEither<ComputationError, FieldSet> = pipe(
  getBooks(__dirname + '/../yaml'),

  // TODO: re-enable when we want to pick-and-choose books
  TaskEither.chain(books =>
    promptTask([
      {
        type: 'checkbox',
        name: 'books',
        message: 'Which rule sets would you like to load?',
        choices: books.map(b => ({
          name: path.basename(b),
          value: b,
        })),
      },
    ])
  ),
  TaskEither.map((x): string[] => x.books),

  TaskEither.chain(load)
);

function buildQuestion(
  ref: Ref,
  choice: Choice,
  resolveRefs: ValueResolver
): Either.Either<ComputationError, DistinctQuestion[]> {
  const k = showRef.show(ref);
  switch (choice.type) {
    case 'freetext':
      return Either.right([{ type: 'input', name: k, message: k }]);
    case 'number':
      return Either.right([{ type: 'number', name: k, message: k }]);
    case 'select': {
      // TODO: think harder about source and target types
      const sources = UnorderedSet.toArray(choice.sources);
      const targets = pipe(choice.targets, resolveRefs);
      if (Option.isNone(targets) || !ListValue.is(targets.value)) {
        return Either.left([
          'select choice cannot target another record value',
        ]);
      }

      const safeTargets = targets.value.list.map(t => ({
        value: t,
        name: showValue.show(t),
        short: showValue.show(t),
      }));

      const pickMany =
        choice.mode === 'one-to-many' || choice.mode === 'many-to-many';
      return Either.right(
        sources.map(
          (source): DistinctQuestion => {
            return {
              type: pickMany ? 'checkbox' : 'list',
              name: k + '.' + source,
              message: k + ': ' + source,
              choices: (answers: Answers) => {
                if (
                  choice.mode === 'many-to-many' ||
                  choice.mode === 'many-to-one'
                ) {
                  return safeTargets;
                }
                const otherAnswers = sources
                  .filter(x => x !== source)
                  .map(s => answers[k] && answers[k][s])
                  .filter(x => x !== undefined);
                const isUsedUp = (t: { value: Value }): boolean =>
                  otherAnswers.some(a =>
                    pickMany ? a.indexOf(t.value) >= 0 : a === t.value
                  );
                return safeTargets.map(t =>
                  isUsedUp(t) ? { ...t, disabled: true } : t
                );
              },
            };
          }
        )
      );
    }
  }
}

// TODO: This is UGLY
function askQuestionsFromChoiceMap(
  choiceMap: Map<Ref, Choice>,
  rootValue: Value,
  rules: FieldSet
): TaskEither.TaskEither<ComputationError, UpdateResult> {
  const resolveRefs = getValueResolver(rootValue);

  const unanswered = pipe(
    choiceMap,
    filterMap(eqRef, ([ref]) => Option.isNone(resolveRefs(ref))),
    Map.toArray(ordRef)
  );

  if (unanswered.length < 1) {
    return TaskEither.right({
      state: rootValue,
      affectedFields: UnorderedSet.empty,
      questions: choiceMap,
    });
  }

  const x = pipe(
    unanswered,
    qs =>
      Array.array.traverse(Either.either)(qs, ([k, choice]) =>
        buildQuestion(k, choice, resolveRefs)
      ),
    Either.map(Array.flatten),
    TaskEither.fromEither,
    TaskEither.chain(promptTask),
    TaskEither.chain(answers =>
      pipe(
        unanswered,
        Array.reduce(
          TaskEither.right<
            ComputationError,
            { root: Value; affected: Set<Ref> }
          >({ root: rootValue, affected: Set.empty }),
          (
            mr,
            [ref, choice]
          ): TaskEither.TaskEither<
            ComputationError,
            { root: Value; affected: Set<Ref> }
          > => {
            switch (choice.type) {
              case 'freetext':
              case 'number':
                return pipe(
                  mr,
                  TaskEither.chain(result =>
                    pipe(
                      answers,
                      atPath<string>(ref.name.split('.')),
                      Option.fromNullable,
                      Option.fold(
                        () =>
                          Either.left(['Could not retrieve inquirer answer']),
                        flow(
                          prim => ({ prim }),
                          ref.set(result.root),
                          Either.map(root => ({
                            root,
                            affected: Set.insert(eqRef)(ref)(result.affected),
                          }))
                        )
                      ),
                      TaskEither.fromEither
                    )
                  )
                );
              case 'select': {
                const sources = UnorderedSet.toArray(choice.sources);

                // TODO: chain this up
                const getAnswerTuple = (
                  prev: Either.Either<ComputationError, Value>,
                  source: string
                ): Either.Either<ComputationError, Value> => {
                  if (Either.isLeft(prev)) {
                    return prev;
                  }
                  const innerRef = monoidRef.concat(ref, fromStringKey(source));
                  const val = atPath<Value>(innerRef.name.split('.'))(answers);
                  if (val === undefined) {
                    return Either.left([
                      'undefined answer for ref ' + showRef.show(innerRef),
                    ]);
                  }
                  return innerRef.set(prev.right)(val);
                };

                // TODO: enforce single picks where necessary
                return pipe(
                  mr,
                  TaskEither.chain(result =>
                    pipe(
                      sources,
                      Array.reduce(Either.right(result.root), getAnswerTuple),
                      Either.map(root => ({
                        root,
                        affected: Set.union(eqRef)(
                          result.affected,
                          Set.fromArray(eqRef)(
                            sources.map(s =>
                              monoidRef.concat(ref, fromStringKey(s))
                            )
                          )
                        ),
                      })),
                      TaskEither.fromEither
                    )
                  )
                );
              }
            }
          }
        )
      )
    ),
    TaskEither.map(update => {
      const affectedFields = pipe(
        update.affected,
        Set.toArray(ordRef),
        Array.map(r => Map.lookup(eqRef)(r, rules)),
        Array.compact,
        Array.map(r => Set.toArray(ordRef)(r.dependants)),
        Array.flatten,
        Set.fromArray(eqRef)
      );
      return {
        state: update.root,
        questions: Map.empty,
        affectedFields,
      };
    })
  );

  return x;
}

const loop = (
  rules: FieldSet,
  c: Value,
  changed: Set<Ref>
): TaskEither.TaskEither<ComputationError, Value> =>
  pipe(
    // update computations and work out which questions to ask
    updateAll(rules, c, changed),
    TaskEither.fromEither,
    // at this point there should be no unresolved computations - just questions to ask
    TaskEither.chain(u =>
      askQuestionsFromChoiceMap(u.questions, u.state, rules)
    ),
    // if we have created any more questions or computations, we will start the loop again
    TaskEither.chain(u => {
      if (u.affectedFields.size < 1) {
        return TaskEither.right(u.state);
      }
      return loop(rules, u.state, u.affectedFields);
    })
  );

const main = pipe(
  setup,
  TaskEither.chain(rs => {
    return loop(
      rs,
      RecordValue.empty,
      Set.fromArray(eqRef)([fromStringKey('game-system')])
    );
  }),
  TaskEither.fold(
    flow(Console.error, Task.fromIO),
    flow(flow(showValue.show, Console.log), Task.fromIO)
  )
);

main();

function filterMap<K, V>(
  eq: Eq<K>,
  f: (i: [K, V]) => boolean
): (m: Map<K, V>) => Map<K, V> {
  return m => {
    let result: Map<K, V> = Map.empty;
    m.forEach((v, k) => {
      if (f([k, v])) {
        result = Map.insertAt(eq)(k, v)(result);
      }
    });
    return result;
  };
}

// Todo: could recurse earlier and avoid ()()
// eslint-disable-next-line @typescript-eslint/no-explicit-any
function atPath<T>(p: string[]): (answers: any) => T | undefined {
  return function(answers) {
    const [head, ...tail] = p;
    if (head) {
      if (answers[head]) {
        return atPath<T>(tail)(answers[head]);
      }
      return undefined;
    }
    return answers;
  };
}

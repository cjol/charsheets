import { fromNullable, getOrElse } from 'fp-ts/lib/Option';
import { pipe } from 'fp-ts/lib/pipeable';

export const tap = (label?: string) => <T>(x: T): T => {
  console.log(label, x);
  return x;
};

export const prop = <A, K extends keyof A>(x: K) => (y: A) => y[x];

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function memoize<A extends any[], B>(
  fn: (...as: A) => B
): (...as: A) => B {
  const cache: Map<A, B> = new Map();
  return function<T>(this: T, ...args: A): B {
    return pipe(
      fromNullable(cache.get(args)),
      getOrElse(() => {
        const val = fn.apply(this, args);
        cache.set(args, val);
        return val;
      })
    );
  };
}

export const stash = <A = unknown>() => (x: A): [A, A] => [x, x];
export const mapStash = <B, C>(f: (b: B) => C) => <A>([a, b]: [A, B]): [
  A,
  C
] => [a, f(b)];
export const unstash = <A, B, C>(f: (a: A, b: B) => C) => ([a, b]: [A, B]) =>
  f(a, b);

A - size of universe of rules
D - average number of dependencies for a rule
Y - average number of dependants for a rule

Operation order:
Prelim:
Set `next` = [], `values` = {}, `rules` = {};

Setup: 
For each rule R:
  - add R to `rules` under the key `rules[R.field].rules`. 
  - For each dependency D1 of R, add R.field to `rules[D1].dependants` list.
  - If R has no dependencies, add R.field to `next`.
   
Evaluation:
If `next` is empty, stop.
Pop a field F from `next`, and...
  - determine the active ruleset (i.e. eliminate all rules where prerequisites are not met). Combine to form derivation function G.
  - If F requires no user input, set values[F] = G(). Otherwise, present the choice to the user and, whenever they respond, set values[F] = choice.
Repeat evaluation (somehow checking for cycles and complaining)

Propagation:
Any time that values[D] is updated, add rules[D].dependencies to `next` (if the value actually changed) and trigger evaluation.
  
# Sample game system
## Rules
### Core rules

```
- field: root/game-system
  type: choice
  choice: 
    type: select
    values:
      - minidnd
- field: minidnd
  type: group
  prerequisites:
    - field: root/game-system # implicitly adds "depends" root/game-system to `depends`
      value: minidnd
- field: minidnd/basic/name
  type: choice
  choice:
    type: text
  prerequisites:
    - field: minidnd # requires that the field be set, but places no restraints on value
- field: minidnd/skills/str
  type: set
  value: 10
  prerequisites:
    - field: minidnd 
- field: minidnd/skills/int
  type: set
  value: 10
  prerequisites:
    - field: minidnd
```
  

### The Wizard Book

```
- field: minidnd/class
  type: choice
  choice: 
    type: select
    values:
      - wizard
  prerequisites:
    - field: minidnd 
- field: minidnd/skills/int
  type: add
  value: 5
  prerequisites:
    - field: minidnd/class
      equals: wizard
```

### The Fighter Book

```
- field: minidnd/class
  type: choice
  choice: 
    type: select
    values:
      - fighter
  prerequisites:
    - field: minidnd 
- field: minidnd/skills/int
  type: add
  value: -2
  prerequisites:
    - field: minidnd/class
      equals: fighter
- field: minidnd/skills/str
  type: add
  value: 7
  prerequisites:
    - field: minidnd/class
      equals: fighter
```

## Sample Evaluation

### Setup

Set  changed = []`, `values = {}`, `rules = {}`;

After setup and assuming all books are loaded, `rules` looks like: 
```
root/game-system
  rules:
    - field: root/game-system
      type: choice
      choice: 
        type: select
        values: ["minidnd"]
  dependants: ["minidnd"]

minidnd:
  rules: 
    - type: group
      depends: 
        - root/game-system
      prerequisites:
        - field: root/game-system 
          value: minidnd
  dependants: 
    - minidnd/basic/name
    - minidnd/skills/int
    - minidnd/skills/str
    - minidnd/class

minidnd/basic/name:
  rules:
    - type: choice
      choice:
        type: text
      depends: ["minidnd"]
      prerequisites: [{field: "minidnd"}]

minidnd/skills/int:
  rules: 
    - type: set
      value: 10
      depends: ["minidnd"]
      prerequisites: [{field: "minidnd"}]
    - type: add
      value: 5
      depends: ["minidnd/class"]
      prerequisites:
        - field: minidnd/class
          equals: wizard
    - type: add
      value: -2
      depends: ["minidnd/class"]
      prerequisites:
        - field: minidnd/class
          equals: fighter

minidnd/skills/str:
  rules:
    - type: set
      value: 10
      depends: ["minidnd"]
      prerequisites: [{field: "minidnd"}]
    - type: add
      value: 7
      depends: ["minidnd/class"]
      prerequisites:
        - field: minidnd/class
          equals: fighter

minidnd/class:
  rules:
    - type: choice
      choice: 
        type: select
        values: ["wizard"]
      depends: ["minidnd"]
      prerequisites: [{field: "minidnd"}]
    - type: choice
      choice: 
        type: select
        values: ["fighter"]
      depends: ["minidnd"]
      prerequisites: [{field: "minidnd"}]
  dependants:
    - minidnd/skills/int
    - minidnd/skills/str

```
`next` looks like `["root/game-system"]`

### Evaluation 1
Pop field `root/game-system` from `next`. Merge all rules (there's only one) to determine that `F` = `ask the user to choose a game system from ["minidnd"]`, so ask them.
`next` is empty so evaluation stops.

### User Interaction 1
User picks `minidnd` as the value for `root/game-system`. 
```
values = {
  "root/game-system": "minidnd"
}

next = ["minidnd"]
```

Trigger evaluation.

### Evaluation 2
Pop field "minidnd" from `next`. Merge all rules (there's only one) which satisfy prerequisites (it does). `F() = true`. Instant evaluation so

```
values = {
  root/game-system: "minidnd",
  minidnd: true
}

next = 
    - minidnd/basic/name
    - minidnd/skills/int
    - minidnd/skills/str
    - minidnd/class
```

Pop field "minidnd/basic/name" from `next`.  Merge all rules (there's only one) which satisfy prerequisites (it does). `F` = `ask the user to enter a name`, so ask them.

```
values = {
  root/game-system: "minidnd",
  minidnd: true
}

next = 
    - minidnd/skills/int
    - minidnd/skills/str
    - minidnd/class
```

Pop field "minidnd/skills/int" from `next`.  Merge all rules which satisfy prerequisites (only the first one does). `F() = 10`. Instant evaluation so

```
values = {
  root/game-system: "minidnd",
  minidnd: true,
  minidnd/skills/int: 10
}

next = 
    - minidnd/skills/str
    - minidnd/class
```

Pop field "minidnd/skills/str" from `next`.  Merge all rules which satisfy prerequisites (only the first one does). `F() = 10`. Instant evaluation so

```
values = {
  root/game-system: "minidnd",
  minidnd: true,
  minidnd/skills/int: 10,
  minidnd/skills/str: 10,
}

next = ["minidnd/class"]
```

Pop field "minidnd/class" from `next`. Merge all rules which satisfy prerequisites (both do). `F = ask the user to choose a class from ["wizard", "fighter"]`, so ask them.

`next` is empty so evaluation stops.


### User Interaction 2
User enters `jonno` as the value for `minidnd/basic/name`.  No dependants so don't add anything to `next`.
```
values = {
  root/game-system: "minidnd",
  minidnd: true,
  minidnd/skills/int: 10,
  minidnd/skills/str: 10,
  minidnd/basic/name: "jonno",
}

next = []
```
Trigger evaluation().

### Evaluation 3

next is empty, so stop. No changes.

### User Interaction 2
User selects `wizard` as the value for `minidnd/class`.  Two dependants to add to `next`.
```
values = {
  root/game-system: "minidnd",
  minidnd: true,
  minidnd/skills/int: 10,
  minidnd/skills/str: 10,
  minidnd/basic/name: "jonno",
  minidnd/class: "wizard"
}

next = ["minidnd/skills/int", "minidnd/skills/str"]
```
Trigger evaluation().

### Evaluation 4

Pop field "minidnd/skills/int" from `next`.  Merge all rules which satisfy prerequisites (the first two do). `F() = 10 + 5`. Instant evaluation so

```
values = {
  root/game-system: "minidnd",
  minidnd: true,
  minidnd/skills/int: 15,
  minidnd/skills/str: 10,
  minidnd/basic/name: "jonno",
  minidnd/class: "wizard"
}

next = ["minidnd/skills/str"]
```

Pop field "minidnd/skills/str" from `next`.  Merge all rules which satisfy prerequisites (only the first one does). `F() = 10`. Instant evaluation so

```
values = {
  root/game-system: "minidnd",
  minidnd: true,
  minidnd/skills/int: 15,
  minidnd/skills/str: 10,
  minidnd/basic/name: "jonno",
  minidnd/class: "wizard"
}

next = []
```
Value is unchanged, so do not add anything to `next`.

Next is empty, so stop.





